# orderByAnswer
Allow to set the order of answers the way you want

## Documentation
Before activate this plugin you need to get and activate [toolsSmartDomDocument plugin](https://framagit.org/SondagePro-LimeSurvey-plugin/toolsDomDocument).

## Copyright
- Copyright © 2017 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
