<?php
/**
 * answersOrdering : set the order of answers the way you want'
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016 Denis Chenu <http://www.sondages.pro>
 * @license AGPL
 * @version 0.0.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class answersOrdering extends PluginBase
{

    static protected $name = 'answersOrdering';
    static protected $description = 'Add an attribute to order single choice question the way you want.';
    /**
    * Did it's already own controller
    */
    private $ownController=false;

    public function init()
    {
        $this->subscribe('beforeActivate');

        $this->subscribe('beforeQuestionRender','answersOrderingFix');
        $this->subscribe('newQuestionAttributes');
    }

    /**
     * Activate or not
     */
    public function beforeActivate()
    {
        $oToolsSmartDomDocument = Plugin::model()->find("name=:name",array(":name"=>'toolsDomDocument'));
        if(!$oToolsSmartDomDocument)
        {
            $this->getEvent()->set('message', gT("You must download <a href='https://framagit.org/SondagePro-LimeSurvey-plugin/toolsDomDocument'>toolsSmartDomDocument</a> plugin"));
            $this->getEvent()->set('success', false);
        }
        elseif(!$oToolsSmartDomDocument->active)
        {
            $this->getEvent()->set('message', gT("You must activate toolsSmartDomDocument plugin"));
            $this->getEvent()->set('success', false);
        }
    }

    public function newQuestionAttributes()
    {
        $event = $this->getEvent();
        $questionAttributes = array(
            'answersOrderingBy'=>array(
                "types"=>"L!FX",
                'category'=>gT('Display'),
                'sortorder'=>101,
                'inputtype'=>'text',
                'default'=>'',
                "help"=>'List of code separated by , or ;. Can be an expression manager. If result is empty : no change was done, This can remove existing answers (only code in list are shown).',
                "caption"=>'Order by code'
            ),
        );
        $event->append('questionAttributes', $questionAttributes);
    }


    public function answersOrderingFix()
    {
        $aAttributes=QuestionAttribute::model()->getQuestionAttributes($this->getEvent()->get('qid'));
        if(isset($aAttributes["answersOrderingBy"]) && trim($aAttributes["answersOrderingBy"])!=="")
        {
            if(!class_exists("toolsDomDocument\SmartDOMDocument")){
                Yii::log("Unable to load SmartDOMDocument Class",'error','application.plugins.answersOrdering');
                return;
            }
            $oEvent=$this->getEvent();
            $answersOrderingBy=templatereplace($aAttributes["answersOrderingBy"]);
            $aNewOrder=explode(",",trim($aAttributes["answersOrderingBy"]));
            if(count($aNewOrder)){
                $dom = new \toolsDomDocument\SmartDOMDocument();
                $dom->loadPartialHTML($this->event->get('answers'));
                switch ($this->event->get('type'))
                {
                    case '!':
                        $sAnswerId="answer{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}";
                        $input=$dom->getElementById($sAnswerId);
                        if($input)
                        {
                            $aOptions=array();
                            foreach ($input->getElementsByTagName('option') as $elOption)
                            {
                                switch($elOption->getAttribute("value"))
                                {
                                    case "":
                                        $emptyPos=count($aOptions);
                                        $emptyElement=$elOption;
                                        break;
                                    case "-oth-":
                                        $otherElement=$elOption;
                                        break;
                                    default:
                                        $aOptions[$elOption->getAttribute("value")]=$elOption;
                                        break;
                                }
                            }
                            while ($input->childNodes->length)
                            {
                                $input->removeChild($input->firstChild);
                            }
                            if(isset($emptyPos) && $emptyPos===0)
                            {
                                $input->appendChild($emptyElement);
                            }
                            foreach($aNewOrder as $value)
                            {
                                if(isset($aOptions[$value]))
                                {
                                    $input->appendChild($aOptions[$value]);
                                }
                            }
                            if(isset($otherElement))
                            {
                                $input->appendChild($otherElement);
                            }
                            if(isset($emptyPos) && $emptyPos>0)
                            {
                                $input->appendChild($emptyElement);
                            }
                        }
                        else
                        {
                            /* replace by Yii::log */
                            tracevar("{$sAnswerId} is not found in HTML produced for answers");
                        }
                        break;
                    case "L":
                        $lineBaseId="javatbd{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}";
                        $aElement=array();
                        $aElementToDelete=array();
                        foreach($aNewOrder as $value)
                        {
                            $line=$dom->getElementById($lineBaseId.$value);
                            if($line)
                            {
                                $aElement[$value]=$line;
                                $wrapperElement=isset($wrapperElement) ? $wrapperElement : $line->parentNode;
                            }
                        }
                        if(isset($wrapperElement))
                        {
                            /* Find if we have an other element */
                            $lineOtherAnswerId="javatbd{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}other"; /* LS version ! */
                            $lineOtherAnswer=$dom->getElementById($lineOtherAnswerId);
                            /* Find if we have an no answer element */
                            $inputNoAnswerId="answer{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}NANS"; /* LS version ! */
                            $inputNoAnswer=$dom->getElementById($inputNoAnswerId);
                            if($inputNoAnswer)
                            {
                                $lineNoAnswer=$inputNoAnswer->parentNode;
                            }
                            /* Add the hidden input */
                            $inputHidden=$dom->getElementById("java{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}");
                            while ($wrapperElement->childNodes->length)
                            {
                                $wrapperElement->removeChild($wrapperElement->firstChild);
                            }
                            foreach($aElement as $line)
                            {
                                $wrapperElement->appendChild($line);
                            }
                            if(isset($lineOtherAnswer))
                            {
                                $wrapperElement->appendChild($lineOtherAnswer);
                            }
                            if(isset($lineNoAnswer))
                            {
                                $wrapperElement->appendChild($lineNoAnswer);
                            }
                            $wrapperElement->appendChild($inputHidden);
                        }
                        break;
                    default:
                }
                $newHtml = $dom->saveHTMLExact();
                $oEvent->set('answers',$newHtml);
            }
        }
    }
}
